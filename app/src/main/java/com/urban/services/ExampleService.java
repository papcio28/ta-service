package com.urban.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.NotificationCompat;

import java.util.UUID;

public class ExampleService extends IntentService {
    private static final String TAG = "ExampleService";

    private NotificationManager mNotificationManager;

    public ExampleService() {
        super("ExampleService");

        // Domyślnie w onStartCommand jest zwracane START_NOT_STICKY
        // Jeżeli chcemy żeby po naglym wylaczeniu uslugi dostarczono nam spowrotem Intenty
        // to robimy :
        setIntentRedelivery(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Wykonuje się na wątku w tle, nigdy na głównym !
        String content = intent.getStringExtra("content");

        Bitmap iconBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentText(content)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(iconBitmap)
                .setContentTitle("Przypomnienie")
                .setTicker(content)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setVibrate(new long[]{0, 100, 100, 200, 100, 50, 400, 50, 100, 200, 500})
                .setLights(0xFF30CBFF, 2000, 1000)
                .build();
        mNotificationManager.notify(UUID.randomUUID().hashCode(), notification);
    }
}









