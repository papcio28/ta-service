package com.urban.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.start_service)
    protected Button mStartButton;
    @BindView(R.id.content)
    protected EditText mContent;
    @BindView(R.id.delay)
    protected Spinner mDelay;

    private AlarmManager mAlarmManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    }

    @OnClick(R.id.start_service)
    protected void onStartClick() {
        String content = mContent.getText().toString();
        int delayValue = Integer.parseInt((String) mDelay.getSelectedItem());

        Long fireDate = System.currentTimeMillis() + (delayValue * 1000);

        Intent serviceIntent = new Intent(this, ExampleService.class);
        serviceIntent.putExtra("content", content);

        PendingIntent pendingIntent = PendingIntent.getService(this,
                fireDate.hashCode(),
                serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        mAlarmManager.set(AlarmManager.RTC_WAKEUP, fireDate, pendingIntent);
    }
}







